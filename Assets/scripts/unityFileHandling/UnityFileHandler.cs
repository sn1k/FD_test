﻿using System.IO;
using UnityEngine;

public static class UnityFileHandler
{
    public static string LoadUnityResourcesTextAsset(string in_path)
    {
        var textAsset = Resources.Load(Path.ChangeExtension(in_path, null)) as TextAsset;

        if (textAsset == null)
            return null;

        return textAsset.text;
    }
}
