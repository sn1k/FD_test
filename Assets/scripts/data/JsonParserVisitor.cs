﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

public interface IJsonParserVisitor
{
    bool Parse(JObject in_rootJsonData, out JsonDataMeta in_container);
    bool Parse(JObject in_rootJsonData, out List<JsonDataPlayer> in_container);
    bool Parse(JObject in_rootJsonData, out JsonDataPlayerImage in_container);
}

public class JsonParserVisitor : IJsonParserVisitor
{
    public bool Parse(JObject in_rootJsonData, out JsonDataMeta in_meta)
    {
        in_meta = new JsonDataMeta();
        
        try
        {
            JObject containerMeta = null;
            FanDuelJsonUtilities.TryLoadObject(in_rootJsonData, "_meta", out containerMeta);

            FanDuelJsonUtilities.TryLoadObject(containerMeta, "_primary_document", out in_meta.m_primaryDocument);

            // simplify object count containers with count child as single entries
            JObject containerCountsPlayer = null;

            if(FanDuelJsonUtilities.TryLoadObject(containerMeta, "players", out containerCountsPlayer))
            {
                FanDuelJsonUtilities.TryLoadObject(containerCountsPlayer, "counts", out in_meta.m_countPlayers);
            }
        }
        catch (JsonException e)
        {
            Console.WriteLine("Exception: " + e.ToString());
            return false;
        }

        return true;
    }


    public bool Parse(JObject in_rootJsonData, out List<JsonDataPlayer> in_container)
    {
        in_container = new List<JsonDataPlayer>();

        JArray containerPlayers = null;
        FanDuelJsonUtilities.TryLoadObject(in_rootJsonData, "players", out containerPlayers);

        var playerEnumerator = containerPlayers.GetEnumerator();

        while (playerEnumerator.MoveNext())
        {
            JObject nextPlayerData = (JObject)playerEnumerator.Current;

            JsonDataPlayer nextPlayer = new JsonDataPlayer();

            try
            {
                string firstname, lastname;
                FanDuelJsonUtilities.TryLoadObject(nextPlayerData, "first_name", out firstname);
                FanDuelJsonUtilities.TryLoadObject(nextPlayerData, "last_name", out lastname);
                
                nextPlayer.m_fullname = string.Format("{0} {1}", firstname, lastname);

                if (FanDuelJsonUtilities.TryLoadObject(nextPlayerData, "fppg", out nextPlayer.m_fppg) == true)
                { }

                JObject containerImages = null;

                // handle images list, expecting only "default"
                if (FanDuelJsonUtilities.TryLoadObject(nextPlayerData, "images", out containerImages))
                {
                    List<JsonDataPlayerImage> images = new List<JsonDataPlayerImage>(containerImages.Count);

                    var enumerator = containerImages.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        var nextObject = enumerator.Current;
                        JsonDataPlayerImage image;

                        if (Parse((JObject)nextObject.Value, out image))
                        {
                            images.Add(image);
                        }
                    }

                    if (images.Count > 0)
                    {
                        nextPlayer.m_playerImages = images;
                    }
                }

                in_container.Add(nextPlayer);
            }
            catch (JsonException e)
            {
                Console.WriteLine("Exception: " + e.ToString());
                return false;
            }
        }

        return true;
    }

    public bool Parse(JObject in_rootJsonData, out JsonDataPlayerImage in_container)
    {
        in_container = new JsonDataPlayerImage();

        try
        {
            FanDuelJsonUtilities.TryLoadObject(in_rootJsonData, "width", out in_container.m_width);
            FanDuelJsonUtilities.TryLoadObject(in_rootJsonData, "height", out in_container.m_height);

            string url;
            FanDuelJsonUtilities.TryLoadObject(in_rootJsonData, "url", out url);

            Uri uri;

            if (Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                in_container.m_url = uri;
            }

        }
        catch (JsonException e)
        {
            Console.WriteLine("Exception: " + e.ToString());
            return false;
        }

        return true;
    }
}
