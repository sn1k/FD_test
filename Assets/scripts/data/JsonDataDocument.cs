﻿
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public class JsonDataDocument
{
    public readonly JsonDataMeta m_meta;
    public readonly List<JsonDataPlayer> m_players;

    public JsonDataDocument(IJsonParserVisitor in_parser, string in_fileContent)
    {        
        JObject root = JObject.Parse(in_fileContent);

        in_parser.Parse(root, out m_meta);
        in_parser.Parse(root, out m_players);
    }
}
