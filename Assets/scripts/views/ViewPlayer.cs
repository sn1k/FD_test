﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ViewPlayer : MonoBehaviour
{
    internal string m_url = string.Empty;

    private Image m_imageComponent;

    public void Awake()
    {
        m_imageComponent = (Image)GetComponent(typeof(Image));
    }

    internal void SetImageURL(string in_url)
    {
        m_url = in_url;

        StartCoroutine(LoadImage());
    }

    internal void ResetImage()
    {
        m_imageComponent.sprite = null;
    }

    private IEnumerator LoadImage()
    {
        if (m_url == string.Empty)
            yield break;
        
        if (m_imageComponent == null)
            yield break;

        WWW www = new WWW(m_url);
        yield return www;

        m_imageComponent.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }
}
