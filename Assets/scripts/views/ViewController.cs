﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewController : MonoBehaviour
{
    // editor linked
    public ViewPlayer m_playerLeft;
    public ViewPlayer m_playerRight;

    public Text m_scoreLabelLeft;
    public Text m_scoreLabelRight;

    public Text m_nameLeft;
    public Text m_nameRight;

    public Text m_successLabel;

    private JsonDataPlayer m_playerDataLeft;
    private JsonDataPlayer m_playerDataRight;

    private List<JsonDataPlayer> m_loadedPlayers;
    private System.Random m_random = new System.Random();

    private int m_score = 0;
    private bool m_waitingForRefresh = false;

    /// <summary>
    /// Assign onclick handlers for the image buttons
    /// </summary>
    public void Awake()
    {
        var buttonLeft = (Button)m_playerLeft.GetComponent(typeof(Button));
        var buttonRight = (Button)m_playerRight.GetComponent(typeof(Button));

        if (buttonLeft != null)
            buttonLeft.onClick.AddListener( () => { OnPlayerClicked(false); } );

        if (buttonRight != null)
            buttonRight.onClick.AddListener(() => { OnPlayerClicked(true); });
    }

    public void Start()
    {
        m_scoreLabelLeft.text = "FPPG Score: ???";
        m_scoreLabelRight.text = "FPPG Score: ???";
        m_successLabel.text = "Score: 0";
    }            

    internal void LoadFromData(JsonDataDocument in_document)
    {
        m_loadedPlayers = in_document.m_players;

        ReloadView();
    }

    /// <summary>
    /// Randomly choose two players from the loaded list and show their details. Make a web request for their images
    /// </summary>
    private void ReloadView()
    {
        int indexLeft = m_random.Next(m_loadedPlayers.Count - 1);
        int indexRight = m_random.Next(m_loadedPlayers.Count - 1);

        // ensure we don't match the same player twice (if unless there's only one available)
        while (m_loadedPlayers.Count > 1 && indexLeft == indexRight)
        {
            indexLeft = m_random.Next(m_loadedPlayers.Count - 1);
            indexRight = m_random.Next(m_loadedPlayers.Count - 1);
        }

        m_playerDataLeft = m_loadedPlayers[indexLeft];
        m_playerDataRight = m_loadedPlayers[indexRight];

        m_nameLeft.text = m_playerDataLeft.m_fullname;
        m_nameRight.text = m_playerDataRight.m_fullname;

        m_playerLeft.SetImageURL(m_playerDataLeft.m_playerImages[0].m_url.AbsoluteUri);
        m_playerRight.SetImageURL(m_playerDataRight.m_playerImages[0].m_url.AbsoluteUri);
        
        m_successLabel.text = string.Format("Score: {0}", m_score);

        m_scoreLabelLeft.text = "FPPG Score: ???";
        m_scoreLabelRight.text = "FPPG Score: ???";
    }
    
    /// <summary>
    /// On click, show scores and result. Trigger a refresh after a bit.
    /// </summary>
    /// <param name="in_rightPlayerPressed"></param>
    internal void OnPlayerClicked(bool in_rightPlayerPressed)
    {
        if (m_waitingForRefresh)
            return;

        m_scoreLabelLeft.text = string.Format("FPPG Score: {0:0.###}", m_playerDataLeft.m_fppg);
        m_scoreLabelRight.text = string.Format("FPPG Score: {0:0.###}", m_playerDataRight.m_fppg);

        bool leftGreater = (m_playerDataLeft.m_fppg > m_playerDataRight.m_fppg);
        bool correct = (leftGreater != in_rightPlayerPressed);

        m_score += (correct ? 1 : 0);

        m_successLabel.text = string.Format("{0}!\nScore: {1}", (correct ? "Correct" : "Wrong"), m_score);

        m_waitingForRefresh = true;
        StartCoroutine(RefreshAfterSeconds(1.8f));
    }

    /// <summary>
    /// Coroutine to handle clearing of labels and the player images after some time
    /// </summary>
    /// <param name="in_seconds"></param>
    /// <returns></returns>
    private IEnumerator RefreshAfterSeconds(float in_seconds)
    {
        yield return new WaitForSeconds(in_seconds);

        m_playerLeft.ResetImage();
        m_playerRight.ResetImage();
        
        ReloadView();

        m_waitingForRefresh = false;

        yield return null;
    }
}
