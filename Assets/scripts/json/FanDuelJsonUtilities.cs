﻿using Newtonsoft.Json.Linq;
using System;

public static class FanDuelJsonUtilities
{
    public static bool TryGetObject<T>(JObject in_root, string in_name, out T out_value)
    {
        JToken jToken;

        // if child exists, assign jtoken
        if (in_root.TryGetValue(in_name, out jToken))
        {
            // key appears but the value is null
            if (jToken.Type == JTokenType.Null)
            {
                out_value = default(T);
                return false;
            }

            try
            {
                out_value = (T)Convert.ChangeType(jToken, typeof(T));
            }
            catch (InvalidCastException)
            {
                out_value = default(T);
                return false;
            }

            return true;
        }

        out_value = default(T);
        return false;
    }

    public static bool TryLoadObject<T>(JObject in_root, string in_name, out T out_value)
    {
        if (TryGetObject(in_root, in_name, out out_value) == false)
        {
            // error handling
            return false;
        }

        return true;
    }
}
