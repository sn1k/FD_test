﻿
using UnityEngine;

public class Main : MonoBehaviour
{
    // editor linked
    public ViewController m_controller;

	public void Start()
    {
        IJsonParserVisitor visitor = new JsonParserVisitor();

        string fileContents = UnityFileHandler.LoadUnityResourcesTextAsset("testData/Player.json");

        JsonDataDocument document = new JsonDataDocument(visitor, fileContents);

        m_controller.LoadFromData(document);
    }
}
