﻿
using NUnit.Framework;

public class TestsUnityFileHandling
{

    /// <summary>
    /// Ensure unity resource loading is working
    /// </summary>
    [Test]
    public void TestUnityResourceLoading()
    {
        string playerjson = UnityFileHandler.LoadUnityResourcesTextAsset("testData/Player.json");
        Assert.IsTrue(playerjson != null);
        Assert.IsTrue(playerjson.Length > 0);

        string parseJson = UnityFileHandler.LoadUnityResourcesTextAsset("testData/ParseTest.json");
        Assert.IsTrue(parseJson != null);
        Assert.IsTrue(parseJson.Length > 0);
    }
}
