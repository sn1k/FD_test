﻿
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

public class TestsJsonData
{
    private string LoadFile(string in_filepath)
    {
        Assert.IsTrue(File.Exists(in_filepath));

        string content;
        using (StreamReader streamReader = new StreamReader(in_filepath))
        {
            content = streamReader.ReadToEnd();
        }

        Assert.IsTrue(content.Length > 0);
        return content;
    }
    
    /// <summary>
    /// Ensure test and runtime data exists and is loadable
    /// </summary>
    [Test]
    public void TestFilesLoadability()
    {
        LoadFile("Assets/Resources/testData/Player.json");
        LoadFile("Assets/Resources/testData/ParseTest.json");
    }

    /// <summary>
    /// Ensures we can extract common types from a parsed json file using our utilities
    /// </summary>
    [Test]
    public void TestUtilityJsonParsing()
    {
        string jsonFilePath = "Assets/Resources/testData/ParseTest.json";
        string content = LoadFile(jsonFilePath);

        JObject root = JObject.Parse(content);

        Assert.IsTrue(root != null);
        Assert.IsTrue(root.Type == JTokenType.Object); // implicit check: value-not-null for found key
        Assert.IsTrue(root.Count == 4);

        string testString;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(root, "string", out testString));
        Assert.IsTrue(testString == "one");

        JObject subObject;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(root, "subObject", out subObject));
        Assert.IsTrue(subObject != null);
        Assert.IsTrue(subObject.Type == JTokenType.Object);
        Assert.IsTrue(subObject.Count == 1);

        int subObjectInt;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(subObject, "int", out subObjectInt));
        Assert.IsTrue(subObjectInt == 2);

        JArray arrayObj;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(root, "array", out arrayObj));
        Assert.IsTrue(arrayObj != null);
        Assert.IsTrue(arrayObj.Type == JTokenType.Array);
        Assert.IsTrue(arrayObj.Count == 2);
        
        JObject elementSecond = (JObject)arrayObj[1];
        Assert.IsTrue(elementSecond != null);
        Assert.IsTrue(elementSecond.Type == JTokenType.Object);

        string intAsString;
        int convertedIntValue;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(elementSecond, "intAsString", out intAsString));
        Assert.IsTrue(int.TryParse(intAsString, out convertedIntValue));
        Assert.IsTrue(convertedIntValue == 4);

        double doubleVal;
        Assert.IsTrue(FanDuelJsonUtilities.TryGetObject(root, "double", out doubleVal));
        Assert.IsTrue(doubleVal == 0.55555555789d);
    }

    /// <summary>
    /// Test some assumptions on document format, namely there's four immediate children and they're optionally populated
    /// </summary>
    [Test]
    public void TestJsonPlayerDocumentValidFormat()
    {
        string jsonPath = "Assets/Resources/testData/Player.json";
        string content = LoadFile(jsonPath);

        JObject root = JObject.Parse(content);

        Assert.IsTrue(root != null);
        Assert.IsTrue(root.Type == JTokenType.Object); // implicit check: value-not-null for found key
        Assert.IsTrue(root.Count == 4);

        List<string> namesRootChildren = new List<string> { "_meta", "fixtures", "players", "teams" };

        var rootEnumerator = root.GetEnumerator();

        while (rootEnumerator.MoveNext())
        {
            Assert.IsTrue(namesRootChildren.Contains(rootEnumerator.Current.Key));
        }
    }
}
